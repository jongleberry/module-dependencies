
# module-dependencies

[![NPM version][npm-image]][npm-url]
[![Build status][travis-image]][travis-url]
[![Test coverage][coveralls-image]][coveralls-url]
[![Dependency Status][david-image]][david-url]
[![License][license-image]][license-url]
[![Downloads][downloads-image]][downloads-url]
[![Gittip][gittip-image]][gittip-url]

Parse ES6 module dependencies.

## Example

```js
var parse = require('module-dependencies')

var imports = parse(`
  import 'asdf'
  import x from '1234'
`)

assert.deepEqual(imports, [
  'asdf',
  '1234'
])
```

## API

### imports = parse(js, [options])

- `js` - string of JavaScript
- `options`
  - `esprima` - custom Esprima implementation, defaulting to `esprima-fb`
  - `filename` - filename for this JS for error reporting

[gitter-image]: https://badges.gitter.im/webdeps/module-dependencies.png
[gitter-url]: https://gitter.im/webdeps/module-dependencies
[npm-image]: https://img.shields.io/npm/v/module-dependencies.svg?style=flat-square
[npm-url]: https://npmjs.org/package/module-dependencies
[github-tag]: http://img.shields.io/github/tag/webdeps/module-dependencies.svg?style=flat-square
[github-url]: https://github.com/webdeps/module-dependencies/tags
[travis-image]: https://img.shields.io/travis/webdeps/module-dependencies.svg?style=flat-square
[travis-url]: https://travis-ci.org/webdeps/module-dependencies
[coveralls-image]: https://img.shields.io/coveralls/webdeps/module-dependencies.svg?style=flat-square
[coveralls-url]: https://coveralls.io/r/webdeps/module-dependencies
[david-image]: http://img.shields.io/david/webdeps/module-dependencies.svg?style=flat-square
[david-url]: https://david-dm.org/webdeps/module-dependencies
[license-image]: http://img.shields.io/npm/l/module-dependencies.svg?style=flat-square
[license-url]: LICENSE
[downloads-image]: http://img.shields.io/npm/dm/module-dependencies.svg?style=flat-square
[downloads-url]: https://npmjs.org/package/module-dependencies
[gittip-image]: https://img.shields.io/gratipay/jonathanong.svg?style=flat-square
[gittip-url]: https://gratipay.com/jonathanong/
