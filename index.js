var format = require('esprima-error-formatter')
var lazy = require('lazyrequire')(require)
var _esprima = lazy('esprima-fb')

module.exports = function (js, options) {
  options = options || {}
  var esprima = options.esprima || _esprima()
  var filename = options.filename || 'anonymous.js'
  var ast
  try {
    ast = esprima.parse(js, {
      sourceType: 'module'
    })
  } catch (err) {
    throw format(err, js, filename)
  }
  var imports = []
  var body = ast.body
  for (var i = 0; i < body.length; i++) {
    var node = body[i]
    if (node.type === 'ImportDeclaration' || node.type === 'ExportDeclaration')
    if (node.source && node.source.type === 'ModuleSpecifier')
    // make the import names unique
    if (!~imports.indexOf(node.source.value))
      imports.push(node.source.value)
  }
  return imports
}
