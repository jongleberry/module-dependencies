
var assert = require('assert')

var parse = require('..')

describe('Parse', function () {
  it('should parse bare imports', function () {
    var js = 'import "asdf"'
    var imports = parse(js)
    assert(imports.length === 1)
    assert.deepEqual(imports, ['asdf'])
  })

  it('should parse multiple bare imports', function () {
    var js = 'import "asdf"; import "1234"'
    var imports = parse(js)
    assert(imports.length === 2)
    assert.deepEqual(imports, ['asdf', '1234'])
  })

  it('should parse default imports', function () {
    var js = 'import x from "asdf"'
    var imports = parse(js)
    assert(imports.length === 1)
    assert.deepEqual(imports, ['asdf'])
  })

  it('should parse named imports', function () {
    var js = 'import {x, y} from "asdf"'
    var imports = parse(js)
    assert(imports.length === 1)
    assert.deepEqual(imports, ['asdf'])
  })

  it('should parse object imports', function () {
    var js = 'import * as fs from "asdf"'
    var imports = parse(js)
    assert(imports.length === 1)
    assert.deepEqual(imports, ['asdf'])
  })

  it('should parse proxied exports', function () {
    var js = 'export * from "asdf"'
    var imports = parse(js)
    assert(imports.length === 1)
    assert.deepEqual(imports, ['asdf'])
  })

  it('should parse named proxied exports', function () {
    var js = 'export { x, y } from "asdf"'
    var imports = parse(js)
    assert(imports.length === 1)
    assert.deepEqual(imports, ['asdf'])
  })
})

describe('Ignores', function () {
  it('should ignore export defaults', function () {
    var js = 'export default 1'
    assert.deepEqual(parse(js), [])
  })

  it('should ignore named exports', function () {
    var js = 'var a = 1; export { a }'
    assert.deepEqual(parse(js), [])
  })

  it('should ignore duplicate imports', function () {
    var js = 'import x from "asdf"; import { y } from "asdf"'
    assert.deepEqual(parse(js), ['asdf'])
  })
})

describe('Error', function () {
  it('should gracefully log syntax errors', function () {
    try {
      parse('export')
    } catch (err) {
      console.error(err.annotated)
      return
    }
    assert(false)
  })
})
